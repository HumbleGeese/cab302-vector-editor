package ve.image.tools;

import java.awt.geom.Point2D;

import ve.image.VectorImage;

/**
 * An interface to provide methods for tools to implement to recieve user input
 * and edit a vector image
 */
public interface Tool {
	/**
	 * Called when the users primary mouse button is pressed
	 * @param vi The image being edited
	 * @param p The point2d in the image space where the mouse was pressed down
	 */
	public void mouseDown(VectorImage vi, Point2D p);
	/**
	 * Called when the users primary mouse button is moved
	 * @param vi The image being edited
	 * @param p The point2d in the image space where the mouse was moved to
	 */
	public void mouseMoved(VectorImage vi, Point2D p);
	/**
	 * Called when the users primary mouse button is pressed and the mouse is moved
	 * @param vi The image being edited
	 * @param p The point2d in the image space where the mouse was dragged to
	 */
	public void mouseDragged(VectorImage vi, Point2D p);
	/**
	 * Called when the users primary mouse button is released
	 * @param vi The image being edited
	 * @param p The point2d in the image space where the mouse was released
	 */
	public void mouseReleased(VectorImage vi, Point2D p);
	/**
	 * Called when the users primary mouse button is pressed
	 * @param vi The image being edited
	 * @param key The key pressed
	 */
	public void keyTyped(VectorImage vi, int key);
}

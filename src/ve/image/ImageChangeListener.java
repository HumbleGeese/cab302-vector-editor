package ve.image;

/**
 * An interface that provides functionality for objects to listen for changes to a vector image
 * @author lowen
 *
 */
public interface ImageChangeListener {
	/**
	 * Called when a vector images content is changed
	 * @param vectorImage The image that was changed
	 */
	public void imageChanged(VectorImage vectorImage);
}

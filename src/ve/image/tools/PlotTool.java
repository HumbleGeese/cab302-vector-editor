package ve.image.tools;

import java.awt.geom.Point2D;

import ve.image.VectorImage;
import ve.image.render.DrawPlotStep;

/**
 * Draws plots on the grid
 * @author lowen
 *
 */
public class PlotTool implements Tool {
	/**
	 * New plot tool
	 */
	public PlotTool() {};
	@Override
	public void mouseDown(VectorImage vi, Point2D p) {
		vi.addStep(new DrawPlotStep(p.getX(), p.getY()));
	}
	@Override
	public void mouseMoved(VectorImage vi, Point2D p) {}
	@Override
	public void mouseDragged(VectorImage vi, Point2D p) {}
	@Override
	public void mouseReleased(VectorImage vi, Point2D p) {}
	@Override
	public void keyTyped(VectorImage vi, int key) {}
}
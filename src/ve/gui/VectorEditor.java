package ve.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import resources.Resources;
import ve.exceptions.VectorImageCodingException;
import ve.gui.components.ToolBar;
import ve.gui.components.VectorImageEditingPanel;
import ve.gui.listeners.ListenerAction;
import ve.image.VectorImage;
import ve.image.VectorImageIO;
import ve.image.tools.LineTool;
import ve.image.tools.Tool;
import ve.misc.VectorFileFilter;

/**
 * Vector editor
 * @author lowen
 */
public class VectorEditor {
	/**
	 * Program entry point
	 * @param args program arguments, ignored
	 */
	public static void main(String[] args) {
		VectorEditor ve = new VectorEditor();
		ve.showWindow();
	}
	/**
	 * The window of the program
	 */
	private JFrame window;
	/**
	 * The content panel of the window holding all the other components
	 */
	private JPanel contentPanel;
	/**
	 * The menu bar of the vector editor with access to other tools
	 */
	private JMenuBar menuBar;
	/**
	 * The toolbar of the vector editor with access to common tools
	 */
	private ToolBar toolbar;
	/**
	 * The jtabbed pane
	 */
	private JTabbedPane tabbedPane;
	/**
	 * The active tool on the editing panel
	 */
	private Tool tool = new LineTool();
	/**
	 * Makes a new vector editor
	 */
	public VectorEditor() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Unable to obtain system look and feel", "Errro", JOptionPane.ERROR_MESSAGE);
		}

		JFrame.setDefaultLookAndFeelDecorated(true);
		
		createWindow();
	}
	/**
	 * Show the window of the vector editor
	 */
	public void showWindow() {
		window.setPreferredSize(new Dimension(525, 600));
		window.setContentPane(contentPanel);
		window.pack();
		window.setMinimumSize(window.getSize());
		window.setLocationRelativeTo(null);
		window.setVisible(true);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	/**
	 * Create the window of the vector editor
	 */
	private void createWindow() {
		window = new JFrame("Vector Editor");
		
		createToolbar();
		createTabbedPane();
		createMenuBar();

		createContentPanel();
	}
	/**
	 * Creates the tabbed pane of the editor
	 */
	private void createTabbedPane() {
		tabbedPane = new JTabbedPane();
		tabbedPane.addTab("Blank", new VectorImageEditingPanel(this, new VectorImage()));
	}
	/**
	 * Creates the toolbar of the editor
	 */
	private void createToolbar() {
		toolbar = new ToolBar(this);
		BoxLayout boxlayout = new BoxLayout(toolbar, BoxLayout.X_AXIS);
		toolbar.setLayout(boxlayout);
		// Set border for the panel
		toolbar.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
	}
	/**
	 * Builds the content panel of the window,
	 * uses a grid bag layout for the components
	 */
	private void createContentPanel() {
		contentPanel = new JPanel();
		contentPanel.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		contentPanel.add(menuBar, gbc);

		gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		contentPanel.add(toolbar, gbc);

		gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1;
		gbc.weighty = 1;
		contentPanel.add(tabbedPane, gbc);
	}
	/**
	 * Builds the menu bar of the vector editor window,
	 * provides toos to save and open files
	 */
	private void createMenuBar() {
		menuBar = new JMenuBar();
		
		buildFileMenu();
		buildPanelMenu();
	}
	/**
	 * Builds the panel menu
	 */
	private void buildPanelMenu() {
		JMenuItem zoomIn = createMenuItem("Zoom In", "images/zoom-in.png");
		JMenuItem zoomOut = createMenuItem("Zoom Out", "images/zoom-out.png");
		zoomIn.addActionListener((e) ->{
			getActiveImageEditor().zoom(true, null);
		});
		zoomOut.addActionListener((e) ->{
			getActiveImageEditor().zoom(false, null);
		});
		JMenu panel = new JMenu("Panel");
		panel.add(zoomIn);
		panel.add(zoomOut);
		menuBar.add(panel);
	}
	/**
	 * Builds the file menu
	 */
	private void buildFileMenu() {
		ActionListener saveActionListener = e -> {saveActiveImage();};
		ActionListener saveAsActionListener = e -> {saveAsActiveImage();};
		ActionListener saveAllActionListener = e -> {saveAllImages();};
		ActionListener openFileActionListener = e -> {openVectorImageFile();};
		ActionListener newFileActionListener = e -> {createNewVectorImage();};
		ActionListener closeFileActionListener = e -> {closeActiveImage();};
		ActionListener closeAllFileActionListener = e -> {closeAllImages();};
		
        JMenuItem newFile = createMenuItem("New", "images/new-file.png");
        JMenuItem open = createMenuItem("Open", "images/open-folder.png");
        JMenuItem saveAs = createMenuItem("Save As", "images/save-as.jpg");
        JMenuItem saveAll = createMenuItem("Save All", "images/saveAll.png");
        JMenuItem save = createMenuItem("Save", "images/save.png");
        JMenuItem close = createMenuItem("Close Active", "images/closeIcon.png");
        JMenuItem closeAll = createMenuItem("Close All", "images/closeIcon.png");
        
        //keyboard shortcut for 'new' button
        KeyStroke keyNewFile = KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK);
        registerShortcut(newFile, keyNewFile, "openNewFile", newFileActionListener);

        //keyboard shortcut for 'open' button
        KeyStroke keyOpen = KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK);
        registerShortcut(open, keyOpen, "openFile", openFileActionListener);

        //keyboard shortcut for 'save' button
        KeyStroke keySave = KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK);
        registerShortcut(save, keySave, "performSave", saveActionListener);
        
        //keyboard shortcut for 'save all' button
        KeyStroke keySaveAll = KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK);
        registerShortcut(saveAll, keySaveAll, "performSaveAll", saveAllActionListener);

		newFile.addActionListener(newFileActionListener);
		open.addActionListener(openFileActionListener);
		save.addActionListener(saveActionListener);
		saveAs.addActionListener(saveAsActionListener);
		saveAll.addActionListener(saveAllActionListener);
		close.addActionListener(closeFileActionListener);
		closeAll.addActionListener(closeAllFileActionListener);

		JMenu file = new JMenu("File");
		file.add(newFile);
		file.add(open);
		file.addSeparator();
		file.add(save);
		file.add(saveAs);
		file.add(saveAll);
		file.addSeparator();
		file.add(close);
		file.add(closeAll);
		menuBar.add(file);
	}
	/**
	 * Closes the active image
	 */
	public void closeActiveImage() {
		VectorImage vi = getActiveImage();
		if(vi.getRendingSteps().size() > 0) {
			saveActiveImage();
		}
		tabbedPane.remove(tabbedPane.getSelectedIndex());
	}
	/**
	 * Closes the active image
	 */
	public void closeAllImages() {
		while(tabbedPane.getTabCount() > 0) {
			tabbedPane.setSelectedIndex(0);
			closeActiveImage();
		}
	}
	/**
	 * Save all open images
	 */
	public void saveAllImages() {
		int originalTab = tabbedPane.getSelectedIndex();
		int numTabs = tabbedPane.getTabCount();
		for(int i = 0; i < numTabs; i++){
			tabbedPane.setSelectedIndex(i);
			saveActiveImage();
		}
		tabbedPane.setSelectedIndex(originalTab);
	}
	/**
	 * Registers a new shortcut for a component
	 * @param component The component to add the shortcut to
	 * @param keyNewFile The keystroke to listen for
	 * @param label The label for the shorcut
	 * @param actionListener The action listener to associate
	 */
	private void registerShortcut(JComponent component, KeyStroke keyNewFile, String label,
			ActionListener actionListener) {
        component.getActionMap().put("openNewFile", new ListenerAction(actionListener));
        component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyNewFile, "openNewFile");
	}
	/**
	 * Opens a vector image file
	 */
	public void openVectorImageFile() {
        File[] fs = chooseFiles(true);
        for (File f : fs) {
            try {
            	openImage(VectorImageIO.load(f));
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(window, "Unable to load the file\n" + e1.getMessage(), "IO Error", JOptionPane.ERROR_MESSAGE);
            } catch (VectorImageCodingException e1) {
                JOptionPane.showMessageDialog(window, "The vector image is corrupted\r\n" + e1.getMessage(), "IO Error", JOptionPane.ERROR_MESSAGE);
            } catch (NoSuchElementException e1) {
                JOptionPane.showMessageDialog(window, "The vector image is empty\r\n" + e1.getMessage(), "IO Error", JOptionPane.ERROR_MESSAGE);
            }
        }
	}
	/**
	 * Creats a new vector image
	 */
	public void createNewVectorImage() {
		openImage(new VectorImage());
	}
	/**
	 * Saves the active image of the editor
	 */
	public void saveActiveImage() {
       	VectorImage vi = getActiveImage();
       	File f = vi.getFile();
       	if(f == null) {
       		saveAs(vi);
       	} else {
       		save(vi);
       	}
	}
	/**
	 * Performs a save-as on the active image;
	 */
	public void saveAsActiveImage() {
		VectorImage vi = getActiveImage();
		if(vi != null) 
			saveAs(vi);
	}
	/**
	 * Asks the user to pick a file, then saves the image to the file
	 * @param vi The image to save
	 */
	public void saveAs(VectorImage vi) {
        File[] fs = chooseFiles(false);
        if(fs.length == 1) {
            try {
            	String path = fs[0].getPath();
            	if(!path.toLowerCase().endsWith(VectorImageIO.getExtension())) {
            		path = path + VectorImageIO.getExtension();
            	}
            	File f = new File(path);
				VectorImageIO.save(vi, f);
                vi.setFile(f);
                tabbedPane.setTitleAt(tabbedPane.getSelectedIndex(), VectorImageIO.getFileNameNoExtension(f));
            } catch (IOException e1) {
                JOptionPane.showMessageDialog(window, "Unable to save the file\n" + e1.getMessage(), "IO Error", JOptionPane.ERROR_MESSAGE);
            } catch (VectorImageCodingException e1) {
                JOptionPane.showMessageDialog(window, "Unrecognized vector elements,\n unable to encode the vector" + e1.getMessage(), "IO Error", JOptionPane.ERROR_MESSAGE);
            }
        }
	}
	/**
	 * Saves a vector image to it's associated file
	 * @param vi The vector image
	 */
	public void save(VectorImage vi) {
		if(vi.getFile() == null) {
			saveAs(vi);
			return;
		}
        try {
			VectorImageIO.save(vi, vi.getFile());
		} catch (IOException e1) {
            JOptionPane.showMessageDialog(window, "Unable to save the file\n" + e1.getMessage(), "IO Error", JOptionPane.ERROR_MESSAGE);
        } catch (VectorImageCodingException e1) {
            JOptionPane.showMessageDialog(window, "Unrecognized vector elements,\n unable to encode the vector" + e1.getMessage(), "IO Error", JOptionPane.ERROR_MESSAGE);
        }
	}
	/**
	 * Creates a menu item with a label and an icon
	 * @param label The label for the menu item
	 * @param icon The path to the icon for the menu item
	 * @return The menu item created
	 */
	private JMenuItem createMenuItem(String label, String icon) {
		return new JMenuItem(label, Resources.loadIcon(icon));
	}
	/**
	 * Asks the user to choose a file for opening/saving
	 * @param open Indicates that the dialog is an open file dialog not save file
	 * @return The file chosen or null if cancelled
	 */
	private File[] chooseFiles(boolean open) {
		JFileChooser jfc = new JFileChooser();
		int dialogType = open ? JFileChooser.OPEN_DIALOG :  JFileChooser.SAVE_DIALOG;
		jfc.setDialogType(dialogType);
		jfc.setDialogTitle("Choose a file");
		jfc.setFileFilter(new VectorFileFilter());
		jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		if(open) jfc.setMultiSelectionEnabled(true);
		else jfc.setMultiSelectionEnabled(false);
		
		int ret;
		if(open) {
			ret = jfc.showOpenDialog(window);
		} else {
			ret = jfc.showSaveDialog(window);
		}
		
		if(ret == JFileChooser.APPROVE_OPTION) {
			return open ? jfc.getSelectedFiles() : new File[] {jfc.getSelectedFile()};
		}
		return new File[0];
	}
	/**
	 * Gets the active vector image being displayed in an editor
	 * @return The active vector image
	 */
	public VectorImage getActiveImage() {
		Component comp = tabbedPane.getSelectedComponent();
		if(comp == null) return null;
		return ((VectorImageEditingPanel) comp).getImage();
	}
	/**
	 * Gets the active image editor
	 * @return The active editor
	 */
	public VectorImageEditingPanel getActiveImageEditor() {
		Component comp = tabbedPane.getSelectedComponent();
		if(comp == null) return null;
		return ((VectorImageEditingPanel) comp);
	}
	/**
	 * Opens an image by adding a tab and editing panel for it to the window
	 * @param vi The image to open in the editor
	 */
	public void openImage(VectorImage vi) {
		String title = "Blank Image";
		if(vi.getFile() != null) {
			title = VectorImageIO.getFileNameNoExtension(vi.getFile());
		}
		VectorImageEditingPanel viep = new VectorImageEditingPanel(this, vi);
		tabbedPane.addTab(title, viep);
        tabbedPane.setSelectedComponent(viep);
	}
	/**
	 * Gets the currently active tool of the vector editor or null if there is none
	 * @return The editors currently selected tool
	 */
	public Tool getTool() {
		return tool;
	}
	/**
	 * Set the tool for the vector editor to use
	 * @param tool The new tool for the editor
	 */
	public void setTool(Tool tool) {
		this.tool = tool;
	}
	/**
	 * Checks if there is currently a tool in use
	 * @return Whether there is a tool selected
	 */
	public boolean isToolSelected() {
		return tool != null;
	}
}

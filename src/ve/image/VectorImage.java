package ve.image;

import java.awt.Graphics2D;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import ve.image.render.VectorRender;
import ve.image.render.VectorRenderStep;

/**
 * A class representing 
 * @author lowen
 *
 */
public class VectorImage implements Iterable<VectorRenderStep> {
	/**
	 * The file associated with this image
	 */
	private File file = null;
	/**
	 * The steps to draw the image
	 */
	private ArrayList<VectorRenderStep> steps = new ArrayList<VectorRenderStep>();
	/**
	 * Image change listeners to be notifed when a change occurs
	 */
	private Set<ImageChangeListener> listeners = new HashSet<>();
	/**
	 * Creates a new empty vector image
	 */
	public VectorImage() {}
	/**
	 * Add a rendering step to the image
	 * @param vrs The rendering step to add
	 */
	public void addStep(VectorRenderStep vrs) {
		if(vrs == null) throw new NullPointerException("Null Vector Render step passed");
		steps.add(vrs);
		notifyListeners();
	}
	/**
	 * Remove the last rendering step in the image
	 * @return The step removed from the image
	 */
	public VectorRenderStep undoStep() {
		if(steps.size() > 0) {
			VectorRenderStep vrs = steps.remove(steps.size() - 1);
			notifyListeners();
			return vrs;
		}
		return null;
	}
	/**
	 * Notifies image change listeners
	 */
	private void notifyListeners() {
		for(ImageChangeListener listener : listeners) {
			listener.imageChanged(this);
		}
	}
	/**
	 * Returns the rendering steps of the image
	 * @return The rendering steps of the image
	 */
	public ArrayList<VectorRenderStep> getRendingSteps() {
		return steps;
	}
	/**
	 * Make it posible to iterator over the render steps of a vector image without accessing the list directly
	 */
	@Override
	public Iterator<VectorRenderStep> iterator() {
		return steps.iterator();
	}
	/**
	 * Gets the file associated with the image or null if there is none
	 * @return The file on the hard disk associated with this image
	 */
	public File getFile() {
		return file;
	}
	/**
	 * Change the file associated with the image
	 * @param file The new file to be associated with the image
	 */
	public void setFile(File file) {
		this.file = file;
	}
	/**
	 * Registers a image change listener for image change events on this image
	 * @param listener The listener to register
	 */
	public void addImageChangeListener(ImageChangeListener listener) {
		listeners.add(listener);
	}
	/**
	 * Draws the image to a graphics instance, the image will always be 1x1 so remember to scale the graphics first!
	 * @param g2d The graphics to draw to
	 */
	public void drawImage(Graphics2D g2d) {
		VectorRender vr = new VectorRender(g2d);
		for(VectorRenderStep step : steps) {
			step.applyStep(vr);
		}
	}
}
package ve.image.tools;

import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * A tool to draw lines in an editor
 * @author lowen
 *
 */
public class LineTool extends BoundingBoxShapeTool {
	/**
	 * New line tool
	 */
	public LineTool() {};
	@Override
	public Shape createShape(Point2D p1, Point2D p2) {
		return new Line2D.Double(p1, p2);
	}
	@Override
	public void updateShape(Shape s, Point2D p1, Point2D p2) {
		Line2D line = (Line2D) s;
		line.setLine(p1, p2);
	}
	@Override
	public String getStepType() {
		return "LINE";
	}
}
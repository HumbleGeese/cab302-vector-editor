package ve.gui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

import resources.Resources;
import ve.gui.VectorEditor;
import ve.gui.listeners.ListenerAction;
import ve.image.render.ChangeColorStep;
import ve.image.tools.EllipseTool;
import ve.image.tools.LineTool;
import ve.image.tools.PlotTool;
import ve.image.tools.PolygonTool;
import ve.image.tools.RectangleTool;

/**
 * Will contain the buttons and other components for drawing on the canvas
 * @author lowen
 *
 */
public class ToolBar extends JPanel implements ComponentListener {
	/**
	 * Generated serial version uid
	 */
	private static final long serialVersionUID = 142832071376897111L;
	/**
	 * The editor assiciated with the toolbar
	 */
	private VectorEditor editor;
	/**
	 * The tools panel
	 */
	private JPanel toolPanel;
	/**
	 * The shapes panel
	 */
	private JPanel shapePanel;
	/**
	 * The split pane
	 */
	private JSplitPane splitPane;
	/**
	 * New toolbar
	 * @param editor The editor the toolbar belongs to
	 */
	public ToolBar(VectorEditor editor) {
		super();
		this.editor = editor;

		setupToobar();
	}
	/**
	 * Sets up the toolbars components
	 */
	private void setupToobar() {
		createToolsPanel();
		createShapePanel();
		
		//create a split pane with the two scroll panes in it
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, shapePanel, toolPanel);
		splitPane.addComponentListener(this);
		add(splitPane);
	}
	/**
	 * Creates the shapes panel
	 */
	private void createShapePanel() {
		//panels for labels and buttons
		shapePanel = new JPanel();
		shapePanel.setLayout(new BorderLayout(10, 0));
		//panels for buttons (added to panels above)
		JPanel shapes = new JPanel(new FlowLayout());
		//label for shapes panel
		JLabel lblShape = new JLabel("Shapes");
		lblShape.setHorizontalAlignment(SwingConstants.CENTER);
		//creates and adds buttons to shapes panel
        JButton drawPlot = new JButton();
        styleAndAddButton("images/point.jpg", drawPlot, shapes);
		JButton drawLine = new JButton();
		styleAndAddButton("images/line.png", drawLine, shapes);
		JButton drawCircle = new JButton();
		styleAndAddButton("images/circle.jpg", drawCircle, shapes);
		JButton drawRect = new JButton();
		styleAndAddButton("images/rectangle.jpg", drawRect, shapes);
		JButton drawPolygon = new JButton();
		styleAndAddButton("images/six-sided-star.png", drawPolygon, shapes);

		//add label and buttons to main shapePanel
		shapePanel.add(lblShape, BorderLayout.NORTH);
		shapePanel.add(shapes, BorderLayout.SOUTH);
		
		//ActionListeners for buttons
        drawPlot.addActionListener((e) -> {editor.setTool(new PlotTool());});
		drawLine.addActionListener((e) -> {editor.setTool(new LineTool());});
		drawCircle.addActionListener((e) -> {editor.setTool(new EllipseTool());});
		drawRect.addActionListener((e) -> {editor.setTool(new RectangleTool());});
		drawPolygon.addActionListener((e) -> {editor.setTool(new PolygonTool());});
	}
	/**
	 * Creates the tools panel
	 */
	private void createToolsPanel() {
		toolPanel = new JPanel();
		toolPanel.setLayout(new BorderLayout(10, 0));

		//label for tools panel
		JLabel lblTools = new JLabel("Tools");
		lblTools.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel tools = new JPanel(new FlowLayout());
		
		ActionListener undoActionListener = e -> {editor.getActiveImage().undoStep();};
		
		JButton undo = new JButton();
		undo.addActionListener(undoActionListener);
		//keyboard shortcut for undo button
		KeyStroke keyUndo = KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK);
		registerShortcut(undo, keyUndo, "performUndo", undoActionListener);
		styleAndAddButton("images/undo.png", undo, tools);
		
		//colorPicker tools
		ColorPicker pen = new ColorPicker(new Color(0, 0, 0, 255));
		styleAndAddButton("images/pen.png", pen, tools);
		ColorPicker fillColour = new ColorPicker(new Color(0, 0, 0, 0));
		styleAndAddButton("images/paint-can.png", fillColour, tools);
		
		//Change image colors on colour change
		pen.addColorChangeListener((c) -> {
			editor.getActiveImage().addStep(new ChangeColorStep(c, false));
		});
		fillColour.addColorChangeListener((c) -> {
			editor.getActiveImage().addStep(new ChangeColorStep(c, true));
		});
		
		//add label and buttons to main toolPanel
		toolPanel.add(lblTools, BorderLayout.NORTH);
		toolPanel.add(tools, BorderLayout.SOUTH);
	}
	/**
	 * Registers a new shortcut for a component
	 * @param component The component to add the shortcut to
	 * @param keyNewFile The keystroke to listen for
	 * @param label The label for the shorcut
	 * @param actionListener The action listener to associate
	 */
	private void registerShortcut(JComponent component, KeyStroke keyNewFile, String label,
			ActionListener actionListener) {
        component.getActionMap().put("openNewFile", new ListenerAction(actionListener));
        component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyNewFile, "openNewFile");
	}
	/**
	 * Formats/styles a jbutton before adding it to the specified panel 
	 * @param imagePath The image to put in the button
	 * @param button The button
	 * @param addToPanel The panel to add the button to
	 */
	private void styleAndAddButton(String imagePath, JButton button, JPanel addToPanel) {
		BufferedImage imgPath = Resources.loadImage(imagePath);
		Image scaleImg = imgPath.getScaledInstance( 40, 40,  java.awt.Image.SCALE_SMOOTH );
		button.setIcon(new ImageIcon(scaleImg));
		button.setMargin(new Insets(0, 0, 0, 0));
		button.setBorder(null);
		addToPanel.add(button);
	}
	/**
	 * Implementation of ComponentListener, fixes layout upon resize
	 */
	@Override
	public void componentResized(ComponentEvent e) {
		splitPane.setDividerLocation(0.5);
		splitPane.repaint();
	}
	/**
	 * Implementation of ComponentListener
	 */
	@Override
	public void componentMoved(ComponentEvent e) {}
	/**
	 * Implementation of ComponentListener
	 */
	@Override
	public void componentShown(ComponentEvent e) {}
	/**
	 * Implementation of ComponentListener
	 */
	@Override
	public void componentHidden(ComponentEvent e) {}
}
package ve.gui.listeners;

import java.awt.Color;

/**
 * An listening interface for an object to listen for the selected color
 * of a color picker being changed
 * @author lowen
 *
 */
public interface ColorChangeListener {
	/**
	 * Called when the color of a color picker is changed
	 * @param c The new color of the color picker
	 */
	public void colorChanged(Color c);
}
/**
 * Package containing miscellaneous code that didn't fit into other packages
 */
package ve.misc;
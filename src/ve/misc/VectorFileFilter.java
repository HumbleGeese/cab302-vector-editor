package ve.misc;

import ve.image.VectorImageIO;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * A file filter that only allows vector files and directories to be displayed
 * @author lowen
 *
 */
public class VectorFileFilter extends FileFilter {
	/**
	 * New vector file filter
	 */
	public VectorFileFilter() {}
	@Override
	public boolean accept(File f) {
		if(f.isDirectory()) return true;
		return f.getName().toLowerCase().endsWith(VectorImageIO.getExtension());
	}
	@Override
	public String getDescription() {
		return ".VEC Files";
	}
}

package ve.gui.components;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JOptionPane;

import ve.gui.listeners.ColorChangeListener;

/**
 * A component extending the jbutton, implements a button that when clicked will ask the user to pick a color
 * @author lowen
 *
 */
public class ColorPicker extends JButton implements ActionListener {
	/**
	 * Generated serial version uid
	 */
	private static final long serialVersionUID = 3939996732721259409L;
	/**
	 * The picked color of the color picked
	 */
	private Color picked = null;
	/**
	 * The set of listeners that are listening for the user to change it's picked color
	 */
	private Set<ColorChangeListener> listeners = new HashSet<>();
	/**
	 * Adds a new color change listener
	 * @param e The color change listener
	 * @return Whether the listener was successfully added
	 */
	public boolean addColorChangeListener(ColorChangeListener e) {
		return listeners.add(e);
	}
	/**
	 * Removes a color change listener from the component
	 * @param o The listener to remove
	 * @return Whether the listener was removed
	 */
	public boolean removeColorChangeListener(ColorChangeListener o) {
		return listeners.remove(o);
	}
	/**
	 * New color picked with provieded default color
	 * @param def The default color of the picked
	 */
	public ColorPicker(Color def) {
		this.picked = def;
		addActionListener(this);
	}
	/**
	 * Implementation of action listener, triggers the color picking dialog
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JColorChooser jcc;
		if(picked == null) {
			jcc = new JColorChooser();
		} else {
			jcc = new JColorChooser(picked);
		}
		String[] options = {"Pick", "Disable Color", "Cancel"};
		int opt = JOptionPane.showOptionDialog(null, jcc, "Pick a color", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
		if(opt == 0) {
			picked = jcc.getColor();
			if(picked.getAlpha() != 255) {
				picked = new Color(picked.getRGB(), false);
			}
			for(ColorChangeListener listener : listeners) {
				listener.colorChanged(picked);
			}
		} else if(opt == 1) {
			picked = null;
			for(ColorChangeListener listener : listeners) {
				listener.colorChanged(picked);
			}
		}
		repaint();
	}
}
/**
 * Custom exception classes for exceptions that may need to be thrown
 * if conditions are not met
 */
package ve.exceptions;
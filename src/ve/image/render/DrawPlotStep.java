package ve.image.render;

import java.awt.geom.Line2D;

/**
 * A step representing drawing a plot on the image grid
 * @author lowen
 *
 */
public class DrawPlotStep implements VectorRenderStep {
	/**
	 * The x of the plot
	 */
	private double x;
	/**
	 * The y of the plot
	 */
	private double y;
	/**
	 * The line representing the plot for rendering purposes, a line with no length
	 */
	private Line2D point;
	/**
	 * Create a new step to draw a plot with given coordinate
	 * @param x The x of the plot
	 * @param y THe y of the plot
	 */
	public DrawPlotStep(double x, double y) {
		this.x = x;
		this.y = y;
		point = new Line2D.Double(x, y, x, y);
	}
	@Override
	public String getStepName() {
		return "PLOT";
	}
	@Override
	public void applyStep(VectorRender vr) {
		vr.draw(point);
	}
	/**
	 * Gets the x coordinate of the plot
	 * @return The x of the plot
	 */
	public double getX() {
		return x;
	}
	/**
	 * Gets the y coordinate of the plot
	 * @return The y of the plot
	 */
	public double getY() {
		return y;
	}
}
package ve.image.render;

import java.awt.Shape;

/**
 * Represents any step drawing a shape in the vector image
 * @author lowen
 *
 */
public class DrawShapeStep implements VectorRenderStep {
	/**
	 * The shape to draw
	 */
	private Shape shape;
	/**
	 * The type of the step
	 */
	private String stepType;
	/**
	 * Createa shape draing step for the given shape
	 * @param s The shape to draw
	 * @param stepType The name associated with the shape drawin during the step
	 */
	public DrawShapeStep(Shape s, String stepType) {
		this.shape = s;
		this.stepType = stepType;
	}
	/**
	 * Draws the shape to the render
	 */
	@Override
	public void applyStep(VectorRender vr) {
		vr.draw(shape);
	}
	/**
	 * Returns the step type the DrawShapeStep was initialized with
	 */
	@Override
	public String getStepName() {
		return stepType;
	}
	/**
	 * Get the shape of the step
	 * @return The shape
	 */
	public Shape getShape() {
		return shape;
	}
}
package ve.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import ve.exceptions.VectorImageCodingException;
import ve.image.VectorImage;
import ve.image.VectorImageIO;

class FileIOTests {
	@Test
	void testLoadEllipseString() {
		String str = "ELLIPSE 0.0 0.0 1.0 1.0";
		try {
			assertEquals(str, VectorImageIO.encodeVectorImage(VectorImageIO.decodeVectorImage(str)));
		} catch (VectorImageCodingException e) {
			e.printStackTrace();
		}
	}
	@Test
	void testLoadLineString() {
		String str = "LINE 0.6 0.2 0.9 0.5";
		try {
			assertEquals(str, VectorImageIO.encodeVectorImage(VectorImageIO.decodeVectorImage(str)));
		} catch (VectorImageCodingException e) {
			e.printStackTrace();
		}
	}
	@Test
	void testLoadRectangleString() {
		String str = "RECTANGLE 0.25 0.2 0.9 0.5";
		try {
			assertEquals(str, VectorImageIO.encodeVectorImage(VectorImageIO.decodeVectorImage(str)));
		} catch (VectorImageCodingException e) {
			e.printStackTrace();
		}
	}
	@Test
	void testLoadFromFile() {
		try {
			String str = "LINE 0.0 0.0 1.0 1.0";
			assertEquals(str, VectorImageIO.encodeVectorImage(VectorImageIO.decodeVectorImage(str)));
		} catch (VectorImageCodingException e) {
			e.printStackTrace();
		}
	}
	@Test
	void testSaveEmptyFile() {
		try {
			File f = new File("src/ve/tests/savetest.vec");
			f.getParentFile().mkdirs();
			VectorImageIO.save(new VectorImage(), new File("src/ve/tests/savetest.vec"));
			boolean exists = f.exists();
			assertEquals(true, exists);
		} catch (VectorImageCodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Test
	void openFileThatDoesntExist() {
		Throwable e = null;
		try {
			File f = new File("doesntexist.vec");
			VectorImageIO.encodeVectorImage(VectorImageIO.load(f));
		} catch (Throwable ex) {
			e = ex;
		}
		assertTrue(e instanceof IOException);
	}

}
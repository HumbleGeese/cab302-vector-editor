package ve.image.render;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;

/**
 * Represents an immediate rendering environment for a vector image, it will be disposed of as soon as the render is complete
 * @author lowen
 *
 */
public class VectorRender {
	/**
	 * The fill color of the render
	 */
	public Color fill = null;
	/**
	 * The stroke color of the step
	 */
	public Color stroke = Color.BLACK;
	/**
	 * The graphics associated with the vector render
	 */
	private Graphics2D g;
	/**
	 * Creates a new vector render with the associated graphics
	 * @param g2d The graphics to render to
	 */
	public VectorRender(Graphics2D g2d) {
		g = g2d;
	}
	/**
	 * Draws a shape to the graphics given the render environment
	 * @param s The shape to draw
	 */
	public void draw(Shape s) {
		if(fill != null) {
			g.setColor(fill);
			g.fill(s);
		}
		if(stroke != null) {
			g.setColor(stroke);
			g.draw(s);
		}
	}
	/**
	 * Disposes of any references the render has
	 */
	public void dispose() {
		g = null;
	}
}
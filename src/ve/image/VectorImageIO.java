package ve.image;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Scanner;

import ve.exceptions.VectorImageCodingException;
import ve.image.render.ChangeColorStep;
import ve.image.render.DrawPlotStep;
import ve.image.render.DrawShapeStep;
import ve.image.render.VectorRenderStep;

/**
 * A utility class providing static methods to encode a vector image and save it to a file
 * @author lowen
 *
 */
public class VectorImageIO {
	/**
	 * The file extension as a string
	 */
	private static final String FileExtension = ".vec";
	/**
	 * The shapes which are encoded only by their bounding boxes
	 */
	private static String[] boundingBoxShapes = {"RECTANGLE", "ELLIPSE"};
	/**
	 * The tools which are encoded only by their bounding boxes
	 */
	private static String[] colorSteps = {"PEN", "FILL"};

	/**
	 * Returns the file extension of a VEC file as a string
	 * @return The file extension of a VEC file as a string
	 */
	public static String getExtension() { return FileExtension; }
	/**
	 * Saves a image into a file
	 * @param vi The vector image to save
	 * @param f The file to save to
	 * @throws IOException May be thrown by IO Operations
	 * @throws VectorImageCodingException May be thrown if unable to encode the vector image
	 */
	public static void save(VectorImage vi, File f) throws IOException, VectorImageCodingException {
		FileWriter fw = new FileWriter(f);
		fw.write(encodeVectorImage(vi));
		fw.close();
	}
	/**
	 * Loads a vector image from a file
	 * @param f The file to load
	 * @return The loaded image
	 * @throws IOException may be thrown by IO Operations
	 * @throws VectorImageCodingException Will be thrown if the content of the file is invalid
	 */
	public static VectorImage load(File f) throws IOException, VectorImageCodingException {
		Scanner s = new Scanner(f);
		s.useDelimiter("$");
		String content;
		try {
			content = s.next();
		} catch(Exception e) {
			content = "";
		}
		s.close();

		VectorImage vi = decodeVectorImage(content);
		vi.setFile(f);
		return vi;
	}
	/**
	 * Loads a vector image from the given strings contents
	 * @param content The text content of the vec file
	 * @return The loaded image
	 * @throws VectorImageCodingException May be thrown if the supplied image contents are not formatted correctly
	 */
	public static VectorImage decodeVectorImage(String content) throws VectorImageCodingException {
		try {
			String[] lines = content.split("(\r\n|\n)");
			
			VectorImage vi = new VectorImage();
			
			for(String line : lines) {
				if(line.length() < 1) continue;
				String[] values = line.split(" +");
				if(values.length > 0) {
					VectorRenderStep vrs = null;
					if(values[0].equalsIgnoreCase("LINE")) {
						vrs = decodeLine(values);
					} else if(itemIn(values[0], boundingBoxShapes)) {
						if(values.length < 5) throw new VectorImageCodingException("Missing values for bounding box shape, expected 4");
						Rectangle2D bounds = getBoundingBox(values);
						Shape s = null;
						if(values[0].equalsIgnoreCase("ELLIPSE")) {
							s = new Ellipse2D.Double(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
						} else if(values[0].equalsIgnoreCase("RECTANGLE")) {
							s = bounds;
						} else {
							throw new VectorImageCodingException("Unrecognized/Handled bounding box shape in vector file");
						}
						vrs = new DrawShapeStep(s, values[0]);
					} else if(itemIn(values[0], colorSteps)) {
						boolean fill = values[0].equalsIgnoreCase("fill");
						if(values.length < 2) throw new VectorImageCodingException("Missing color for " + values[0] + " step");
						Color c = readColor(values[1]);
						vrs = new ChangeColorStep(c, fill);
					} else if(values[0].equalsIgnoreCase("polygon")) {
						vrs = decodePolygon(values);
					} else if(values[0].equalsIgnoreCase("plot")) {
						if(values.length < 3) throw new VectorImageCodingException("Expected at least 1 complete coordinate for point");
						double x, y;
						x = Double.parseDouble(values[1]);
						y = Double.parseDouble(values[2]);
						vrs = new DrawPlotStep(x, y);
					} else {
						throw new VectorImageCodingException("Unrecognized step: " + values[0]);
					}
					if(vrs != null) {
						vi.addStep(vrs);
					}
				}
			}
			
			return vi;
		} catch(Exception e) {
			throw new VectorImageCodingException(e);
		}
	}
	/**
	 * Decodes polygon into a render step
	 * @param values The polygon coordinate values
	 * @return The polygon rendering step
	 * @throws VectorImageCodingException May be thrown if there is an error with the input
	 */
	private static VectorRenderStep decodePolygon(String[] values) throws VectorImageCodingException {
		int n = values.length - 1;
		if(n % 2 != 0) throw new VectorImageCodingException("Expected multiple of 2 values for polygon");
		if(n < 2) throw new VectorImageCodingException("Expected at least 1 point for polygon");
		Path2D path = new Path2D.Double();
		for(int i = 1; i < values.length; i += 2) {
			double x, y;
			x = Double.parseDouble(values[i]);
			y = Double.parseDouble(values[i + 1]);
			if(i == 1) {
				path.moveTo(x, y);
			} else {
				path.lineTo(x, y);
			}
		}
		path.closePath();
		return new DrawShapeStep(path, "POLYGON");
	}
	/**
	 * Decodes a line into a line drawing step
	 * @param values The coordinates for the line
	 * @return The render step to draw the line
	 * @throws VectorImageCodingException Thrown if there is an issue with the values provided for the line 
	 */
	private static VectorRenderStep decodeLine(String[] values) throws VectorImageCodingException {
		if(values.length < 5) throw new VectorImageCodingException("Missing values for a LINE");
		double x1, y1, x2, y2;
		try {
			x1 = Double.parseDouble(values[1]);
			y1 = Double.parseDouble(values[2]);
			x2 = Double.parseDouble(values[3]);
			y2 = Double.parseDouble(values[4]);
		} catch (NumberFormatException e) {
			throw new VectorImageCodingException(e);
		}
		return new DrawShapeStep(new Line2D.Double(x1, y1, x2, y2), "LINE");
	}
	/**
	 * Reads a color or 'OFF'
	 * @param string The color as a #xxxxxx string or OFF
	 * @return The color or null if the string was 'OFF'
	 */
	private static Color readColor(String string) {
		if(string.equalsIgnoreCase("off")) return null;
		
		int r, g, b;

		r = Integer.parseInt(string.substring(1, 3), 16);
		g = Integer.parseInt(string.substring(3, 5), 16);
		b = Integer.parseInt(string.substring(5, 7), 16);
		
		return new Color(r, g, b);
	}
	/**
	 * Gets the bounding box from a set of values
	 * @param values The valeus of the bounding box
	 * @return The bounding box
	 */
	private static Rectangle2D getBoundingBox(String[] values) {
		double x1, y1, x2, y2;
		x1 = Double.parseDouble(values[1]);
		y1 = Double.parseDouble(values[2]);
		x2 = Double.parseDouble(values[3]);
		y2 = Double.parseDouble(values[4]);
		return new Rectangle2D.Double(x1, y1, x2 - x1, y2 - y1);
	}
	/**
	 * Saves a image into a file
	 * @param vi The vector image to save
	 * @return A string representing the vector image
	 * @throws VectorImageCodingException May be thrown if the program is unable to encode the vector file
	 */
	public static String encodeVectorImage(VectorImage vi) throws VectorImageCodingException {
		StringBuilder sb = new StringBuilder();
		
		Iterator<VectorRenderStep> steps = vi.iterator();
		
		while(steps.hasNext()) {
			VectorRenderStep vrs = steps.next();
			
			String stepEncoded = encodeStep(vrs);
			
			sb.append(stepEncoded);
			
			if(steps.hasNext()) {
				sb.append("\r\n");
			}
		}
		
		return sb.toString();
	}
	/**
	 * Encodes a vector image render step as a string for the VEC file format
	 * @param vrs The render step to encode
	 * @return The encoeded render set
	 * @throws VectorImageCodingException May be thrown if an error occurs while encoding the step
	 */
	private static String encodeStep(VectorRenderStep vrs) throws VectorImageCodingException {
		ChangeColorStep ccs = null;
		DrawShapeStep dss = null;
		DrawPlotStep dps = null;
		if(vrs instanceof ChangeColorStep) {
			ccs = (ChangeColorStep) vrs;
		}
		if(vrs instanceof DrawShapeStep) {
			dss = (DrawShapeStep) vrs;
		}
		if(vrs instanceof DrawPlotStep) {
			dps = (DrawPlotStep) vrs;
		}
		if(ccs != null) {
			return ccs.getStepName() + " " + colorToHex(ccs.getColor());
		} else if (dss != null) {
			Shape s = dss.getShape();
			if(dss.getStepName().equalsIgnoreCase("LINE")) {
				return dss.getStepName() + " " + encodeLine((Line2D) s);
			} else if(itemIn(dss.getStepName(), boundingBoxShapes)) {
				return dss.getStepName() + " " + boundingBox(s);
			} else if(dss.getStepName().equalsIgnoreCase("POLYGON")) {
				Path2D p2d = (Path2D) s;
				PathIterator pi = p2d.getPathIterator(AffineTransform.getScaleInstance(1, 1));
				double[] coords = new double[6];
				StringBuilder sb = new StringBuilder();
				sb.append("POLYGON");
				while(!pi.isDone()) {
					int t = pi.currentSegment(coords);
					if(t == PathIterator.SEG_MOVETO || t == PathIterator.SEG_LINETO) {
						sb.append(" ");
						sb.append(formatDouble(coords[0]));
						sb.append(" ");
						sb.append(formatDouble(coords[1]));
					} else if(t == PathIterator.SEG_CLOSE) {
						break;
					} else {
						throw new VectorImageCodingException("Unrecognized Path2D steps");
					}
					pi.next();
				}
				return sb.toString();
			} else {
				throw new VectorImageCodingException("Unable to encode step: " + vrs.getStepName());
			}
		} else if(dps != null) {
			return "PLOT " + formatDouble(dps.getX()) + " " + formatDouble(dps.getY());
		}
		throw new VectorImageCodingException("Unable to encode step: " + vrs.getStepName() + ", " + vrs.getClass());
	}
	/**
	 * Encodes a line drawing step
	 * @param s The line to encode
	 * @return The line encoded
	 */
	private static String encodeLine(Line2D s) {
		return formatDouble(s.getX1()) + " " + formatDouble(s.getY1()) + " " + formatDouble(s.getX2()) + " " + formatDouble(s.getY2());
	}
	/**
	 * Returns if a string exists in a set of strings, ignoring case
	 * @param str The string to check for
	 * @param strings The set of strings to search
	 * @return Whether the string was found in the set
	 */
	private static boolean itemIn(String str, String[] strings) {
		for(String str2 : strings) {
			if(str.equalsIgnoreCase(str2)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * Returns string representation of the bounding box of a shape
	 * @param s The shape
	 * @return The string representatino of the bounding box
	 */
	private static String boundingBox(Shape s) {
		Rectangle2D rect = s.getBounds2D();
		StringBuilder sb = new StringBuilder();
		sb.append(formatDouble(rect.getX()));
		sb.append(" ");
		sb.append(formatDouble(rect.getY()));
		sb.append(" ");
		sb.append(formatDouble(rect.getX() + rect.getWidth()));
		sb.append(" ");
		sb.append(formatDouble(rect.getY() + rect.getHeight()));
		return sb.toString();
	}
	/**
	 * Encodes a color into a hex code or OFF if null
	 * @param c The color to encode
	 * @return The color as either "OFF" or a hex color as "#xxxxxx"
	 */
	private static String colorToHex(Color c) {
		return 	(c == null) ? "OFF" : "#" + hex(c.getRed(), 2) + hex(c.getGreen(), 2) + hex(c.getBlue(), 2);
	}
	/**
	 * The hex chars in order
	 */
	private static char[] hexChars = "0123456789ABCDEF".toCharArray();
	/**
	 * Returns the provided integer encoded in base 16 (hex)
	 * @param val the value to encode
	 * @param nibbles the number of bytes of the integer to encode
	 * @return The hex code of the color
	 */
	private static String hex(int val, int nibbles) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < nibbles; i++) {
			int h = val & 0xF;
			char c = hexChars[h];
			sb.append(c);
			val >>>= 4;
		}
		return sb.toString();
	}
	/**
	 * Formats a double with non-scientific notation and as few decimal places as required (no trailing zeros);
	 * @param d The double to format
	 * @return The formatted double as as string
	 */
	private static String formatDouble(double d) {
		return BigDecimal.valueOf(d).toPlainString();
	}
	/**
	 * Returns the files name without the extension
	 * @param f The file
	 * @return The file name as a string without its extension
	 */
	public static String getFileNameNoExtension(File f) {return f.getName().substring(0, f.getName().lastIndexOf('.')); }
}

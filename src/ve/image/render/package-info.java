/**
 * Implementation of anything related to the rendering pipline of vector images
 */
package ve.image.render;
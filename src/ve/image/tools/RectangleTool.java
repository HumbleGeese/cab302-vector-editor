package ve.image.tools;

import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * A tool to draw rectangles, based on the bounding box shape tool
 * @author lowen
 *
 */
public class RectangleTool extends BoundingBoxShapeTool {
	/**
	 * New rectangle tool
	 */
	public RectangleTool() {}
	@Override
	public Shape createShape(Point2D p1, Point2D p2) {
		Rectangle2D rect = new Rectangle2D.Double();
		rect.setFrameFromDiagonal(p1, p2);
		return rect;
	}
	@Override
	public void updateShape(Shape s, Point2D p1, Point2D p2) {
		Rectangle2D rect = (Rectangle2D) s;
		rect.setFrameFromDiagonal(p1, p2);
	}
	@Override
	public String getStepType() {
		return "RECTANGLE";
	}
}
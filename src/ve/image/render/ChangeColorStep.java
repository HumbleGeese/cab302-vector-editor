package ve.image.render;

import java.awt.Color;

/**
 * Represents changing a color in the vector image
 * @author lowen
 *
 */
public class ChangeColorStep implements VectorRenderStep {
	/**
	 * The new color
	 */
	private Color color;
	/**
	 * Whether it is the fill color or stroke color
	 */
	private boolean fill;
	/**
	 * Create a new color change step for the given color
	 * @param c The new color
	 * @param fill Whether it is the fill or stroke color
	 */
	public ChangeColorStep(Color c, boolean fill) {
		this.color = c;
		this.fill = fill;
	}
	/**
	 * Applies the color change to the render
	 */
	@Override
	public void applyStep(VectorRender vr) {
		if(fill) {
			vr.fill = color;
		} else {
			vr.stroke = color;
		}
	}
	@Override
	public String getStepName() {
		if(fill) {
			return "FILL";
		} else {
			return "PEN";
		}
	}
	/**
	 * Gets the color of the step
	 * @return The color
	 */
	public Color getColor() {
		return color;
	}
}
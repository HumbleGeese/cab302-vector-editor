package ve.exceptions;

/**
 * An exception encapsulating any error that may occur while encoding or decoding
 * a vector image file
 * @author lowen
 *
 */
public class VectorImageCodingException extends Exception {
	/**
	 * Generated serial version uid
	 */
	private static final long serialVersionUID = 778809700866169943L;
	/**
	 * New exception based on a message
	 * @param msg The message of the exception
	 */
	public VectorImageCodingException(String msg) {
		super(msg);
	}
	/**
	 * New exception based on another exception
	 * @param e The other exception
	 */
	public VectorImageCodingException(Exception e) {
		super(e);
	}
}
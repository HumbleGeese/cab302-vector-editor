package ve.image.tools;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

/**
 * An tool to draw ellipses, based on the bounding box shape tool
 * @author lowen
 *
 */
public class EllipseTool extends BoundingBoxShapeTool {
	/**
	 * New ellipse tool
	 */
	public EllipseTool() {}
	@Override
	public Shape createShape(Point2D p1, Point2D p2) {
		return new Ellipse2D.Double(p1.getX(), p1.getY(), p2.getX() - p1.getX(), p2.getY() - p1.getY());
	}
	@Override
	public void updateShape(Shape s, Point2D p1, Point2D p2) {
		Ellipse2D ellipse = (Ellipse2D) s;
		ellipse.setFrameFromDiagonal(p1, p2);
	}
	@Override
	public String getStepType() {
		return "ELLIPSE";
	}
}

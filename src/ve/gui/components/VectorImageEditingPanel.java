package ve.gui.components;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import javax.swing.JPanel;

import ve.gui.VectorEditor;
import ve.image.ImageChangeListener;
import ve.image.VectorImage;
import ve.image.tools.Tool;

/**
 * Displays a vector image and allows editing
 * @author lowen
 *
 */
public class VectorImageEditingPanel extends JPanel implements ImageChangeListener, ComponentListener, MouseListener, MouseWheelListener, MouseMotionListener, KeyListener {
	/**
	 * Generated serial version uid
	 */
	private static final long serialVersionUID = -707686582363767589L;
	/**
	 * The scaling factor used when zooming
	 */
	private static double scaleFactor = 1.2;
	/**
	 * The vector image being displayed by the panel
	 */
	private VectorImage vimg;
	/**
	 * The smallest dimension of the panel
	 */
	private double size = 400;
	/**
	 * The current scale of the panel
	 */
	private double scale = 1;
	/**
	 * The current x translation
	 */
	private double tx;
	/**
	 * The current y translation
	 */
	private double ty;
	/**
	 * The point the mouse went down on screen
	 */
	private Point2D downPoint = null;
	/**
	 * The mouse button being pressed down
	 */
	private int downButton = 0;
	/**
	 * The vector editor the panel is associated with
	 */
	private VectorEditor vectorEditor;
	/**
	 * Creates a new vector image editing panel for the supplied image
	 * @param vectorEditor The vector editor to associate the panel with
	 * @param vimg The vector image to edit
	 */
	public VectorImageEditingPanel(VectorEditor vectorEditor, VectorImage vimg) {
		super();
		this.vectorEditor = vectorEditor;
		this.vimg = vimg;
		vimg.addImageChangeListener(this);
		setPreferredSize(new Dimension(400, 400));
		addComponentListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		addMouseWheelListener(this);
		addKeyListener(this);
	}
	/**
	 * Returns the image of the panel
	 * @return The image
	 */
	public VectorImage getImage() {
		return vimg;
	}
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setColor(getBackground());
		g2d.fillRect(0, 0, getWidth(), getHeight());
		g2d.transform(AffineTransform.getScaleInstance((size - 1) * scale, (size - 1) * scale));
		g2d.translate(tx, ty);
		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, 1, 1);
		g2d.setStroke(new BasicStroke((float) (1d / (size) / scale)));

		vimg.drawImage(g2d);
	}
	@Override
	public void imageChanged(VectorImage vi) {
		repaint();
	}
	/**
	 * Implementation of ComponentListener, changes the scaling info if the panel is resized
	 */
	@Override
	public void componentResized(ComponentEvent e) {
		size = (getWidth() < getHeight()) ? getWidth() : getHeight();
		repaint();
	}
	/**
	 * Implementation of ComponentListener
	 */
	@Override
	public void componentMoved(ComponentEvent e) {}
	/**
	 * Implementation of ComponentListener
	 */
	@Override
	public void componentShown(ComponentEvent e) {}
	/**
	 * Implementation of ComponentListener
	 */
	@Override
	public void componentHidden(ComponentEvent e) {}
	/**
	 * Implementation of MouseListener
	 */
	@Override
	public void mouseClicked(MouseEvent e) {}
	/**
	 * Implementation of MouseListener, triggers tools upon activation conditions
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON1) {
			if(vectorEditor.isToolSelected()) {
				Tool tool = vectorEditor.getTool();
				tool.mouseDown(vimg, screenToCanvas(e.getPoint()));
			}
		}
		downPoint = e.getPoint();
		downButton = e.getButton();
		this.requestFocus();
	}
	/**
	 * Implementation of MouseListener, triggers tools upon activation conditionsc
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON1) {
			if(vectorEditor.isToolSelected()) {
				Tool tool = vectorEditor.getTool();
				tool.mouseReleased(vimg, screenToCanvas(e.getPoint()));
			}
			repaint();
		}
	}
	/**
	 * Implementation of MouseMotionListener, triggers tools upon activation conditions, also handles panning
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		if(downButton == MouseEvent.BUTTON3) {
			double dx = e.getX() - downPoint.getX();
			double dy = e.getY() - downPoint.getY();

			dx /= (size - 1);
			dy /= (size - 1);

			dx /= scale;
			dy /= scale;
			
			tx += dx;
			ty += dy;
			
			downPoint = e.getPoint();
			repaint();
		} if(downButton == MouseEvent.BUTTON1) {
			if(vectorEditor.isToolSelected()) {
				Tool tool = vectorEditor.getTool();
				tool.mouseDragged(vimg, screenToCanvas(e.getPoint()));
			}
			repaint();
		}
	}
	/**
	 * Implementation of MouseMotionListener, triggers tools upon activation conditions
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		if(vectorEditor.isToolSelected()) {
			Tool tool = vectorEditor.getTool();
			tool.mouseMoved(vimg, screenToCanvas(e.getPoint()));
		}
	}
	/**
	 * Translates a point on screen to a point on the drawing canvas
	 * @param p The point on screen
	 * @return The point on the drawing canvas
	 */
	public Point2D screenToCanvas(Point2D p) {
		double x = p.getX();
		double y = p.getY();
		x /= (size-1);
		y /= (size-1);
		x /= scale;
		y /= scale;
		x -= tx;
		y -= ty;
		return new Point2D.Double(x, y);
	}
	/**
	 * Implementation of MouseListener
	 */
	@Override
	public void mouseEntered(MouseEvent e) {}
	/**
	 * Implementation of MouseListener
	 */
	@Override
	public void mouseExited(MouseEvent e) {}
	/**
	 * Implementation of MouseWheelListener, handles zooming using scroll wheel
	 */
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		Point2D p = e.getPoint();
		p = screenToCanvas(p);
		int r = e.getWheelRotation();
		zoom(r < 0, p);
	}
	/**
	 * Zomes the image editing panel, either in or out centres on a location
	 * @param in Whether to zoom in or out
	 * @param centre The centre of the zooming
	 */
	public void zoom(boolean in, Point2D centre) {
		if(centre == null) {
			centre = screenToCanvas(new Point2D.Double(getWidth() / 2, getHeight() / 2));
		}
		tx -= centre.getX() / scale;
		ty -= centre.getY() / scale;
		if(in) {
			scale *= scaleFactor;
		} else {
			scale /= scaleFactor;
		}
		scale = Math.max(scale, 1d / 1.2);
		tx += centre.getX() / scale;
		ty += centre.getY() / scale;
		tx = Math.max(-1, Math.min(2, tx));
		ty = Math.max(-1, Math.min(2, ty));
		repaint();
	}
	/**
	 * Implementation of KeyListener
	 */
	@Override
	public void keyTyped(KeyEvent e) {}
	/**
	 * Implementation of KeyListener, triggers tools upon activation conditions
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if(vectorEditor.isToolSelected()) {
			Tool tool = vectorEditor.getTool();
			tool.keyTyped(vimg, e.getKeyCode());
		}
		repaint();
	}
	/**
	 * Implementation of KeyListener
	 */
	@Override
	public void keyReleased(KeyEvent e) {}
}

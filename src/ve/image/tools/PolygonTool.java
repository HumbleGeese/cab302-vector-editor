package ve.image.tools;

import java.awt.event.KeyEvent;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

import ve.image.VectorImage;
import ve.image.render.DrawShapeStep;

/**
 * A tool to draw polygons, based on Path2D
 * @author lowen
 *
 */
public class PolygonTool implements Tool {
	/**
	 * The current path/polygon being draw by the tool
	 */
	private Path2D path = null;
	/**
	 * New polygon tool
	 */
	public PolygonTool() {};
	@Override
	public void mouseDown(VectorImage vi, Point2D p) {
		if(path == null) {
			path = new Path2D.Double();
			path.moveTo(p.getX(), p.getY());
			vi.addStep(new DrawShapeStep(path, "POLYGON"));
		} else {
			path.lineTo(p.getX(), p.getY());
		}
	}
	@Override
	public void mouseMoved(VectorImage vi, Point2D p) {}
	@Override
	public void mouseDragged(VectorImage vi, Point2D p) {}
	@Override
	public void mouseReleased(VectorImage vi, Point2D p) {}
	@Override
	public void keyTyped(VectorImage vi, int key) {
		if(key == KeyEvent.VK_ESCAPE && path != null) {
			path.closePath();
			path = null;
		}
	}

}

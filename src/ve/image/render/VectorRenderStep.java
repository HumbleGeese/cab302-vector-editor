package ve.image.render;

/**
 * An interface for an object to implement for it to be rendered in a vector image
 * @author lowen
 *
 */
public interface VectorRenderStep {
	/**
	 * Converts the step into a string
	 * @return The step represented as a one line string
	 */
	public String getStepName();
	/**
	 * Applies the step to the provided graphics
	 * @param vr The vector render to apply the step to
	 */
	public void applyStep(VectorRender vr);
}
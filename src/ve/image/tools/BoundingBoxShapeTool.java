package ve.image.tools;

import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import ve.image.VectorImage;
import ve.image.render.DrawShapeStep;

/**
 * Represents an tool for drawing any shape based on some bounding box,
 * it also makes the shapes visible while being drawn
 * @author lowen
 *
 */
public abstract class BoundingBoxShapeTool implements Tool {
	/**
	 * The start of the currently being drawn shape
	 */
	private Point2D start = null;
	/**
	 * The currently being drawn shape
	 */
	private Shape shape = null;
	/**
	 * Creates a new bounding box shape tool
	 */
	public BoundingBoxShapeTool() {}
	@Override
	public void mouseDown(VectorImage vi, Point2D p) {
		start = p;
	}
	@Override
	public void mouseMoved(VectorImage vi, Point2D p) {}
	@Override
	public void mouseDragged(VectorImage vi, Point2D p) {
		if(start == null) {
			start = p;
		} else {
			if(shape == null) {
				shape = createShape(start, p);
				vi.addStep(new DrawShapeStep(shape, getStepType()));
			} else {
				updateShape(shape, start, p);
			}
		}
	}
	@Override
	public void mouseReleased(VectorImage vi, Point2D p) {
		if(shape != null) {
			updateShape(shape, start, p);
		}
		start = null;
		shape = null;
	}
	/**
	 * Creates a rectangular bounds based on two corner points, utility method for subclasses
	 * @param p1 The first corner
	 * @param p2 The second corner
	 * @return The bounding rectangle
	 */
	public Rectangle2D getBounds(Point2D p1, Point2D p2) {
		double minX, minY, maxX, maxY;
		minX = Math.max(0, Math.min(p1.getX(), p2.getX()));
		minY = Math.max(0, Math.min(p1.getY(), p2.getY()));
		maxX = Math.min(1, Math.max(p1.getX(), p2.getX()));
		maxY = Math.min(1, Math.max(p1.getY(), p2.getY()));
		return new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
	}
	@Override
	public void keyTyped(VectorImage vi, int key) {}
	/**
	 * Creates a shape based on some initial bounding box
	 * @param p1 The first corner
	 * @param p2 The second corner
	 * @return The shape
	 */
	public abstract Shape createShape(Point2D p1, Point2D p2);
	/**
	 * Updates an existing shape based on a new bounding box
	 * @param s The shape to update
	 * @param p1 The first corner
	 * @param p2 The second corner
	 */
	public abstract void updateShape(Shape s, Point2D p1, Point2D p2);
	/**
	 * Returns the name of the step to be associated with the the shape,
	 * passed to the associated DrawShapeStep
	 * @return The type of the step
	 */
	public abstract String getStepType();
}
package ve.gui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;

/**
 * An action that calls a single action listener
 * @author lowen
 *
 */
public class ListenerAction extends AbstractAction {
	/**
	 * Generate serial version uid
	 */
	private static final long serialVersionUID = -19801356244615167L;
	/**
	 * The ac tion listener to notify
	 */
	private ActionListener actionListener;
	/**
	 * New listener action based on an action listener
	 * @param actionListener The action listener to notify
	 */
	public ListenerAction(ActionListener actionListener) {
		this.actionListener = actionListener;
	}
	/**
	 * Calls the underlying action listener
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		actionListener.actionPerformed(e);
	}
}

package ve.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

import org.junit.jupiter.api.Test;

import ve.exceptions.VectorImageCodingException;
import ve.gui.VectorEditor;
import ve.gui.components.VectorImageEditingPanel;
import ve.image.VectorImage;
import ve.image.VectorImageIO;
import ve.image.tools.*;

public class ToolBarTests {
    @Test
    void testPlotCreation() {
    	VectorEditor ve = new VectorEditor();
        VectorImage vimg = new VectorImage();
        VectorImageEditingPanel viep = new VectorImageEditingPanel(ve, vimg);
        viep.setSize(200,200);
        ve.setTool(new PlotTool());
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 0, 0, 0, false, MouseEvent.BUTTON1));

        try {
            String str = VectorImageIO.encodeVectorImage(vimg);
            String expected = "PLOT 0.0 0.0";
            assertEquals(expected, str);
        } catch (VectorImageCodingException e) {
            fail("Could not encode line after drawing with tool");
        }
    }
    @Test
    void testLineCreation() {
    	VectorEditor ve = new VectorEditor();
        VectorImage vimg = new VectorImage();
        VectorImageEditingPanel viep = new VectorImageEditingPanel(ve, vimg);
        viep.setSize(200, 200);
        ve.setTool(new LineTool());
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 0, 0, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_DRAGGED, System.currentTimeMillis(), 0, 199, 199, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, 199, 199, 0, false, MouseEvent.BUTTON1));
        try {
			String str = VectorImageIO.encodeVectorImage(vimg);
			String expected = "LINE 0.0 0.0 1.0 1.0";
			assertEquals(expected, str);
		} catch (VectorImageCodingException e) {
			fail("Could not encode line after drawing with tool");
		}
    }
    @Test
    void testRectangleCreation() {
    	VectorEditor ve = new VectorEditor();
        VectorImage vimg = new VectorImage();
        VectorImageEditingPanel viep = new VectorImageEditingPanel(ve, vimg);
        viep.setSize(200, 200);
        ve.setTool(new RectangleTool());
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 0, 0, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_DRAGGED, System.currentTimeMillis(), 0, 199, 199, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, 199, 199, 0, false, MouseEvent.BUTTON1));
        try {
			String str = VectorImageIO.encodeVectorImage(vimg);
			String expected = "RECTANGLE 0.0 0.0 1.0 1.0";
			assertEquals(expected, str);
		} catch (VectorImageCodingException e) {
			fail("Could not encode line after drawing with tool");
		}
    }
    @Test
    void testEllipseCreation() {
    	VectorEditor ve = new VectorEditor();
        VectorImage vimg = new VectorImage();
        VectorImageEditingPanel viep = new VectorImageEditingPanel(ve, vimg);
        viep.setSize(200, 200);
        ve.setTool(new EllipseTool());
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 0, 0, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_DRAGGED, System.currentTimeMillis(), 0, 199, 199, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, 199, 199, 0, false, MouseEvent.BUTTON1));
        try {
			String str = VectorImageIO.encodeVectorImage(vimg);
			String expected = "ELLIPSE 0.0 0.0 1.0 1.0";
			assertEquals(expected, str);
		} catch (VectorImageCodingException e) {
			fail("Could not encode line after drawing with tool");
		}
    }
    @Test
    void testPolygonCreation() {
    	VectorEditor ve = new VectorEditor();
        VectorImage vimg = new VectorImage();
        VectorImageEditingPanel viep = new VectorImageEditingPanel(ve, vimg);
        viep.setSize(201, 201);
        ve.setTool(new PolygonTool());
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 0, 0, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, 0, 0, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 25, 25, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, 25, 25, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 75, 75, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, 75, 75, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 100, 100, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, 100, 100, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 0, 0, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, 0, 0, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new KeyEvent(viep, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_ESCAPE, '\0'));
        viep.dispatchEvent(new KeyEvent(viep, KeyEvent.KEY_RELEASED, System.currentTimeMillis(), 0, KeyEvent.VK_ESCAPE, '\0'));
        try {
            String str = VectorImageIO.encodeVectorImage(vimg);
            String expected = "POLYGON 0.0 0.0 0.125 0.125 0.375 0.375 0.5 0.5 0.0 0.0";
            assertEquals(expected, str);
        } catch (VectorImageCodingException e) {
            fail("Could not encode line after drawing with tool");
        }
    }
    @Test
    void testUndoTool() {
    	VectorEditor ve = new VectorEditor();
        VectorImage vimg = new VectorImage();
        VectorImageEditingPanel viep = new VectorImageEditingPanel(ve, vimg);
        viep.setSize(200, 200);
        ve.setTool(new EllipseTool());
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 0, 0, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_DRAGGED, System.currentTimeMillis(), 0, 199, 199, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, 199, 199, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, 0, 0, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_DRAGGED, System.currentTimeMillis(), 0, 199, 199, 0, false, MouseEvent.BUTTON1));
        viep.dispatchEvent(new MouseEvent(viep, MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, 199, 199, 0, false, MouseEvent.BUTTON1));
        viep.getImage().undoStep();
        try {
            String str = VectorImageIO.encodeVectorImage(vimg);
            String expected = "ELLIPSE 0.0 0.0 1.0 1.0";
            assertEquals(expected, str);
        } catch (VectorImageCodingException e) {
            fail("Could not encode line after drawing with tool");
        }
    }
    @Test
    void testPenColourPicker() {
    }
    @Test
    void testFillColourPicker() {
    }
}


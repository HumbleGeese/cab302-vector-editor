package resources;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.UIManager;

/**
 * A utility class that provides static methods to load resources associated with the project
 * @author lowen
 *
 */
public class Resources {
	/**
	 * Hidden constructor
	 */
	private Resources() {};
	/**
	 * Loads an icon from the programs resources
	 * @param path The relative path to the icon's image
	 * @return The loaded icon
	 */
	public static Icon loadIcon(String path) {
		try {
			return new ImageIcon(loadImage(path));
		} catch (Exception e) {
			return UIManager.getIcon("OptionPane.errorIcon");
		}
	}
	/**
	 * Loads an image from the programs resources
	 * @param path The relative path to the image
	 * @return The loaded image
	 */
	public static BufferedImage loadImage(String path) {
		try {
			return ImageIO.read(Resources.class.getResourceAsStream(path));
		} catch (IOException e) {
			return null;
		}
	}
}